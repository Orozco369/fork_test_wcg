`timescale 1ns / 1ps
//`include "func/CeilLog2.vh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:48:18 07/26/2017 
// Design Name: 
// Module Name:    counter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module counter #(parameter CNT_MOD = 160)//** [160] , [200],  [80], [2^7]
(
	input ena,
	input clk,
	input clr,
	input reset,
	input [CeilLog2(CNT_MOD)-1 : 0] MAX,
	
	output  [CeilLog2(CNT_MOD)-1 : 0] cnt,
	output  hit
);

reg hit_;
reg n_hit;
reg [CeilLog2(CNT_MOD)-1 : 0] cnt_;
reg [CeilLog2(CNT_MOD)-1 : 0] n_cnt;

always@(posedge clk, negedge reset)begin
	if (~reset) begin
		cnt_ <= 0;
		hit_ <= 1'b0;	
	end
	else if(clr) begin
		cnt_ <= 0;
		hit_ <= 1'b0;	
	end
	else if (ena) begin 
		cnt_ <= n_cnt;
		hit_ <= n_hit;	
	end
	else begin
		cnt_ <= cnt_ ;
		hit_ <= hit_;
	end
end

always@(*) begin 
	if (cnt_ == (MAX-1'b1)) begin
		n_hit	= 1'b1;
	end
	else begin
		n_hit	= 1'b0;
	end
	
	if (cnt_ < (MAX)) begin
		n_cnt = cnt_ + 1'b1;
	end
	else begin
		n_cnt = 0;
	end
end

assign hit = hit_;
assign cnt = cnt_;

function integer CeilLog2;
  input integer data;
  integer i, result;
  	  begin
	  result = 1; 
		  for (i = 0; 2**i < data; i = i + 1)
			result = i + 1; 
			CeilLog2 = result;
	  end 
  endfunction
  
endmodule
