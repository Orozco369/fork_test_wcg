`timescale 1ns / 1ps


module TB_MOFDM_IP_80211;

	// Inputs
	reg clk;
	reg rst_a;
	reg en_s;
	reg [4:0] conf_dbus;
	reg start;
	reg read;
	reg write;
	reg [31:0] data_in;
	reg availableOBuff_i;
	
	// Outputs
	wire [31:0] data_out;
	wire int_req;
	wire [33:0] OFDMsample_o;
	wire	validOFDMsample_o;

	// Instantiate the Unit Under Test (UUT)
	MOFDM_IP_v1_1 uut (
		.availableOBuff_i(availableOBuff_i),
		.clk(clk), 
		.rst_a(rst_a), 
		.en_s(en_s), 
		.conf_dbus(conf_dbus), 
		.start(start), 
		.read(read), 
		.write(write), 
		.data_in(data_in), 
		.data_out(data_out), 
		.OFDMsample_o(OFDMsample_o),
		.validOFDMsample_o(validOFDMsample_o),
		.int_req(int_req)
	);

	always begin
		clk = ~clk;
		#5;
	end
	
	
	
	parameter STAT_REG  		= 5'b11110;
	parameter ID_IP   		= 5'b11111;
	
	parameter FRM_CR			= 5'b00000;	//0
	parameter SLT_CR			= 5'b00110; //6
	parameter D_REG			= 5'b00001;	//1
	parameter P_REG 			= 5'b00100;	//4
	parameter D_REG_WR_PTR  = 5'b00011;	//3
	parameter P_REG_WR_PTR 	= 5'b00101;	//5
	parameter FRM_CR_PTR 	= 5'b00010; //2
	parameter SLT_CR_PTR 	= 5'b00111; //7
	
	
	

                
	//integer file;
	//integer file2;				 
	integer c;
	
	initial begin
		//file  = $fopen("./TB/o_real.txt","w+");
		//file2 = $fopen("./TB/o_imag.txt","w+");
		// Initialize Inputs
		c = 0;
		clk = 1;
		rst_a = 0;
		en_s = 0;
		conf_dbus = 0;
		start = 0;
		read = 0;
		write = 0;
		data_in = 0;
		availableOBuff_i = 1;
		
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rst_a = 1'b1;
		en_s = 1'b1;
		#10;
		// LECTURA IP-ID
			conf_dbus = ID_IP;
			read = 1'b1; 
			#10;
			read = 1'b0; 
			#10;
		
		//ESCRITURA STATUS REGISTER
			data_in =32'b0000000000000001_0000000000000000;// done|_|no mask
			conf_dbus = STAT_REG;
			write = 1'b1; 
			#10;
			write = 1'b0; 
			#10;
		//lectura STATUS REGISTER
			conf_dbus = STAT_REG;
			read = 1'b1; 
			#10;
			read = 1'b0; 
			#10;
			#10;
			#10;
		// -------------------------------------------------------------- //
		// ---------  Escritura FRM_CR
		// -------------------------------------------------------------- //
			conf_dbus = FRM_CR;
			/*
				// ---------------------------------
				// 		word1 Format
				// ---------------------------------
				parameter WI_NPREAM_WIDTH				= 'd4,											3
				parameter WI_NSLTS_WIDTH				= 'd5,											5
				parameter WI_NSYMB_WIDTH				= 'd5,											1
				parameter WI_FFTLENGTH_WIDTH			= CeilLog2(CeilLog2(MAXFFTLENGTH)),		FFT68
			*/
			data_in 	= 32'b0011_00101_00001_0101_00000000000000; // word1
			write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			
				// ---------------------------------
				// 		wordN Format
				// ---------------------------------
				/*
					parameter WN_SGUARD_WIDTH				= 'd9,
					parameter WN_CP_WIDTH					= 'd9,
					parameter WN_RB_LENGTH_WIDTH			= 'd4,
					parameter WN_RDMODE_WIDTH				= 'd2,
					parameter WN_MODMODE_WIDTH				= 'd2,
					parameter WN_DATAINIADDR_WIDTH		= 'd6
				*/
			// preamb 1: - format: NULL
			data_in = 32'b000001000_000010000_1111_00_00_000000; // 8|16|15|0|0|0|	// 
			write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			// preamb 2: - format 802.11 STS
			data_in = 32'b000001000_000010000_0100_10_01_000000; // 8|16|4|0|1|0|	//  RDMODE: 10: (CP + FFT)x2 ;  1:4QAM 
			write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			// preamb 3: - format 802.11 LTS
			data_in = 32'b000000110_000100000_1111_11_00_000001; // 6|32|15|3|0|0|	// RDMODE: 11: CP + (FFT)x2 ;  0:BPSK
			write 	= 1'b1; 																		
			#10;
			write 	= 1'b0; 
			#10;
			// payload OFDM symb - format
			data_in = 32'b000000110_000010000_1110_00_01_000011; // 6|16|14|0|1|1|	//1:4QAM
			write 	= 1'b1; 																			// LTS ocupo 2 palabras por eso addr = 3
			#10;
			write 	= 1'b0; 
			#10;
	// -------------------------------------------------------------- //
	// --------   Escritura SLT_CR
	// -------------------------------------------------------------- //
			// Format
			/*
					Each two bits define null/data/pilot carrier
					last two bits define DCmode: 0:bypass pattern; 1: stop pattern
			*/
			conf_dbus = SLT_CR;
			data_in = 32'b00_00_00_00_000000000000000000000000;	// preamb 1 (15 nulos)
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			data_in = 32'b01_00_00_00_000000000000000000000000;	// preamb 2 (1 dato y 3 nulos)
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			data_in = 32'b01010101010101010101010101010100;			// preamb 3 (15 datos)
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			data_in = 32'b01_01_01_01_01_10_01_01_01_01_01_01_01_01_0000;	// OFDMsy 1 (5D| 1P| 8D)
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
	// -------------------------------------------------------------- //
	// --------   Escritura D_REG
	// -------------------------------------------------------------- //
			// ----------------------------
			// preamb 2
			// ----------------------------
			conf_dbus = D_REG;
			data_in = 32'b10_01_10_01_01_10_01_01_10_10_10_10_00000000;	// preamb STS 802.11
			
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			// ----------------------------
			// preamb 3
			// ----------------------------
			conf_dbus = D_REG;
			data_in = 32'b11001101011111100110101111100110;	// preamb LTS 802.11	
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			data_in = 32'b10100000110010101111_000000000000;	// preamb LTS 802.11
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
			// ----------------------------
			// OFDMsy
			conf_dbus = D_REG;
			for ( c = 0 ; c < 10 ; c = c+1) begin
				//data_in = 32'b00_01_10_11_00_01_10_11_00_01_10_11_00_01_10_11;	// payload
				data_in = 32'b00000000000000000000000000000000;	// payload
				write 	= 1'b1; 
				#10;
				write 	= 1'b0; 
				#10;
				data_in = 32'b01010101010101010101010101010101;	// payload
				write 	= 1'b1; 
				#10;
				write 	= 1'b0; 
				#10;
				data_in = 32'b10101010101010101010101010101010;	// payload
				write 	= 1'b1; 
				#10;
				write 	= 1'b0; 
				#10;
				data_in = 32'b11111111111111111111111111111111;	// payload
				write 	= 1'b1; 
				#10;
				write 	= 1'b0; 
				#10;
			end
			
			
	// -------------------------------------------------------------- //
	// --------   Escritura P_REG
	// -------------------------------------------------------------- //
			
			conf_dbus = P_REG;
			data_in = 32'b01010101010101010101010101010101;	 
		  	write 	= 1'b1; 
			#10;
			write 	= 1'b0; 
			#10;
	// ______________________________________________________________
	// INICIO PROCESAMIENTO
	// ______________________________________________________________
	start = 1'b1;
	#10;
	start = 1'b0;	
	
	



//$fclose(file); 
//$fclose(file2); 
#20000; //3000 * N
$stop;
	end
      
endmodule

